
#[link_section = ".text"]
static SHELLCODE: [u8; 6] = [0xb8, 0x0a, 0x00, 0x00, 0x00, 0xc3];

fn main() {
    unsafe {
        let shellcode_function: fn() -> i32 = std::mem::transmute(&SHELLCODE);
        let val = shellcode_function();
        println!("{}", val);
    }
}
