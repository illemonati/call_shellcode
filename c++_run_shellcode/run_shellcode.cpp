#include <iostream>

const char shellcode[] = "\xb8\x0a\x00\x00\x00\xc3";
int main() {
  auto shellcode_function = reinterpret_cast<int(*) ()>(shellcode);
  int val = shellcode_function();
  std::cout << val << std::endl;
}
